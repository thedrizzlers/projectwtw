Sub-groups

*Core networking(Cy and Ryan)

*current conditions /RadarSatellite (luca and enrique)

*RadarSatellite(michael and jon)

*Radar/satelittle animation/image(luca and enrique)

*Main window integration (cy and ryan)




  List of Getters:

  Constructor:

*  **Core a = new Core(String s);** 

       // s  is the zipcode for the city you're interested in 

  Methods:

*  **a.getCity()** 

     //returns the city (string)

*  **a.getWeather()**
 
    //returns a quick description of the current weather (string)

*  **a.getTemperature()**

     //returns the current temp in F. (double)

*  **a.getLastUpdated()**

     //returns when the current report was last updated (string)

*  **a.getFeelsLike()**

      //returns what it feels like currently in F (double)

*  **a.getWindMph()**

     //returns the current wind in mph (double)

*  **a.getIcon()**

     //returns the name of the icon that should be used (string)

*  **a.getHowMuchRain()**

    //returns a string of how much it rained that day (string)

*  **a.getForecastDay(int)**
    
    //enter an int 0-4

    //0: current day

    //1: next day

    //2: next next day etc.

    //returns the name of the weekday (string)

*  **a.getForecastDayHigh(int)**
    
    //enter int 0-4

    //returns High of that day (double)

*  **a.getForecastDayLow(int)**

    //enter int 0-4

    //returns low of that day (double)

*  **a.getForecastDayWeather(int)**

    //enter int 0-4

    //returns Conditions. (ex.clear, cloudy, rain.) (String)

* **a.getForecastDayWind(int)**

    //enter int 0-4

    //returns MPH of wind (double)

* **a.getRadarURL()**
   
    //returns the url of the radar image (String)
