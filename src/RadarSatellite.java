// Forecast panel


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class RadarSatellite extends JPanel  {

    public RadarSatellite(Core a){


        JPanel vp = new JPanel();

        try {
            URL urlRadar = new URL(a.getRadarURL());
            ImageIcon image = new ImageIcon(urlRadar);


            BufferedImage bimg = ImageIO.read(urlRadar);
            int width          = bimg.getWidth();
            int height         = bimg.getHeight();

            JLabel label = new JLabel();
            vp.setPreferredSize(new Dimension(400, 350));
            vp.setBackground(a.backround());
            vp.add(label);
            add(vp);
            label.setPreferredSize(new Dimension(400,350));
            label.setIcon(image);
            label.setHorizontalAlignment((int)label.CENTER_ALIGNMENT);
            label.setVerticalAlignment((int)label.CENTER_ALIGNMENT);



            /*JFrame jf = new JFrame("Test");
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jf.add(vp);
            jf.setSize(width + 100, height+100);
            jf.setVisible(true);*/


        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
   /* public static void main(String args[]){
        Core a =new Core ("95747");
        RadarSatellite f = new RadarSatellite(a);*/
}