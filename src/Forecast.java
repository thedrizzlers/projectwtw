// Forecast panel
import acm.gui.VPanel;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;


public class Forecast extends VPanel {

    public Forecast(Core c){
        VPanel jp = new VPanel();
        ImageIcon image = new ImageIcon("images/"+c.getIcon()+".gif");


        //Set up default table model
        String[] colnam = {"Images", "Date", "Temp"};
        Object[][] data = {
                {image, c.getForecastDay(1), c.getForecastDayHigh(1)+" / "+c.getForecastDayLow(1)},
                {image, c.getForecastDay(2), c.getForecastDayHigh(2)+" / "+c.getForecastDayLow(2)},
                {image, c.getForecastDay(3), c.getForecastDayHigh(3)+" / "+c.getForecastDayLow(3)},
                {image, c.getForecastDay(4), c.getForecastDayHigh(4)+" / "+c.getForecastDayLow(4)},
                {image, c.getForecastDay(5), c.getForecastDayHigh(5)+" / "+c.getForecastDayLow(5)},
        };
        DefaultTableModel model = new DefaultTableModel(data, colnam);
        JTable table = new JTable(model){
            //**overrides default function so that images can be rendered
            public Class getColumnClass(int col){
                return getValueAt(0, col).getClass();
            }
        };


        //Set up panel and table
        jp.setBackground(c.backround());
        jp.add(table);
        jp.setPreferredSize(new Dimension(400,260));

        table.setRowHeight(45);
        table.setShowGrid(false);
        table.setBackground(c.backround());
        add(jp);



    }

}