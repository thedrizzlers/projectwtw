import acm.graphics.*;
import acm.program.Program;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProjectWTW extends Program {
    JButton go,clear;
    JTextField zipfield;
    JLabel ziplabel;


    public ProjectWTW()
    {
        start();
        setSize(new Dimension(400,1000));
        setLocation(500,0);
    }

    public void init( )
    {


        GCanvas canvas = new GCanvas();
        add(canvas);
        go = new JButton("Go");
        clear = new JButton("clear");
        zipfield = new JTextField();
        ziplabel = new JLabel("Zipcode:");
        zipfield.setPreferredSize(new Dimension(150,30));
        go.setPreferredSize(new Dimension(50,30));
        clear.setPreferredSize(new Dimension(80,30));
        canvas.add(go, 250, 0);
        canvas.add(zipfield, 100, 0);
        canvas.add(ziplabel,0,0);
        canvas.add(clear,300,0);





        go.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                String what = ae.getActionCommand();

                if (what.equals("Go")) {
                    try {

                        Core a = new Core(zipfield.getText());
                        go.setVisible(false);
                        zipfield.setVisible(false);
                        CurrentConditions cc = new CurrentConditions(a);
                        Forecast forecast = new Forecast(a);
                        RadarSatellite radar = new RadarSatellite(a);
                        canvas.add(radar, 0, 600);
                        canvas.add(cc, 0, 50);
                        canvas.add(forecast, 0, 350);
                        go.setVisible(true);
                        zipfield.setVisible(true);
                    }
                    catch (java.lang.NullPointerException npe)
                    {
                        npe.printStackTrace();
                        zipfield.setText("not a valid zipcode");
                    }



                }

            }



        });
        clear.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                String what = ae.getActionCommand();

                if(zipfield.getText() != "") {


                    if (what.equals("clear")) {
                        canvas.removeAll();
                        canvas.add(go, 250, 0);
                        canvas.add(zipfield, 100, 0);
                        canvas.add(ziplabel,0,0);
                        canvas.add(clear,300,0);
                        zipfield.setText("");

                    }
                }



                }





        });
    }



    public static void main(String[] args)
    {
        ProjectWTW g = new ProjectWTW();
    }
}