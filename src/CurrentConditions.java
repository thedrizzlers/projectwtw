import acm.gui.VPanel;


import acm.gui.TableLayout;
import acm.gui.VPanel;
import acm.program.Program;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class CurrentConditions extends VPanel {


    private JLabel mickey1, Currenttemp, Feelslike,Weather,clothes,umb;


    public CurrentConditions(Core a ) {

        TableLayout table = new TableLayout(9, 9);
        setLayout(table);


        VPanel vp = new VPanel();

        Weather = new JLabel("<html><h1>" + a.getWeather() + "</h1></html>");
        Weather.setHorizontalAlignment(SwingConstants.CENTER);

        Currenttemp = new JLabel("<html><h3>Current: " + a.getTemperature() + "</h3></html>");
        Currenttemp.setHorizontalAlignment(SwingConstants.CENTER);

        Feelslike = new JLabel("<html><h3>Feels like: " + a.getFeelsLike() + "</h3></html>");
        Feelslike.setHorizontalAlignment(SwingConstants.CENTER);

        clothes = new JLabel("<html><h2>" + a.clothes() + "</h2></html>");
        clothes.setHorizontalAlignment(SwingConstants.CENTER);

        umb = new JLabel("");
        umb.setHorizontalAlignment(SwingConstants.CENTER);
        //Current Weather's Icon
        mickey1 = new JLabel(" ");

        mickey1.setHorizontalAlignment(SwingConstants.CENTER);

        String icon = a.getIcon();
        icon.replaceAll("\\s", "");
        ImageIcon curr = new ImageIcon("images/" + icon + ".gif");

        mickey1.setIcon(curr);
        if (a.umb() == true)
            umb.setText("Bring Umbrella");



        vp.add(Weather);
        vp.add(clothes);


        vp.add(umb);


        vp.add(mickey1);
        vp.add(Currenttemp);
        vp.add(Feelslike);


        vp.setOpaque(true);
        Color back = a.backround();
        vp.setBackground(back);
        vp.setPreferredSize(new Dimension(400,300));
        add(vp);


    }




}