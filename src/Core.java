import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import acm.graphics.GImage;
import com.google.gson.*;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Core {
    // Bitly OAuth access token (a unique ID).
    // You can register for your own access token, or use the one here.
    private final String ACCESS_TOKEN = "ca51a08ea2deeeca";
    private JsonElement jse, jse2,jse3;

    public Core(String ZIP) {
        jse = null;
        jse2 = null;

        try {

            // Construct Bitly API URL
            URL URL1 = new URL("http://api.wunderground.com/api/"
                    + ACCESS_TOKEN + "/conditions/q/"
                    + ZIP + ".json");

            // Open the URL
            InputStream is = URL1.openStream(); // throws an IOException
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Read the result into a JSON Element
            jse = new JsonParser().parse(br);

            //second URL for the forecast

            String city = jse.getAsJsonObject().get("current_observation")
                    .getAsJsonObject().get("display_location")
                    .getAsJsonObject().get("city").getAsString();
            String State = jse.getAsJsonObject().get("current_observation")
                    .getAsJsonObject().get("display_location")
                    .getAsJsonObject().get("state").getAsString();

            String city1 = city.replaceAll(" ", "_");
            URL URL2 = new URL("http://api.wunderground.com/api/"
                    + ACCESS_TOKEN + "/forecast10day/q/" + State + "/" + city1 + ".json");

            InputStream is2 = URL2.openStream();
            BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));

            jse2 = new JsonParser().parse(br2);






            URL URL3 = new URL("http://api.wunderground.com/api/"
                    + ACCESS_TOKEN + "/radar/q/" + ZIP + ".json");

            InputStream is3 = URL3.openStream();
            BufferedReader br3 = new BufferedReader(new InputStreamReader(is3));

            jse3 = new JsonParser().parse(br3);

            is3.close();
            br3.close();

            is2.close();
            br2.close();

            is.close();
            br.close();




        }

        catch (java.io.UnsupportedEncodingException uee)
        {
            uee.printStackTrace();
        } catch (java.net.MalformedURLException mue)
        {
            mue.printStackTrace();
        } catch (java.io.IOException ioe)
        {
            ioe.printStackTrace();
        }
        //return null;
    }

    public String getWeather()
    {

        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("weather").getAsString();

    }

    public String getTemperature()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("temp_f").getAsString();
    }

    public String getCity()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("full").getAsString();
    }
    public int getZIP()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("zip").getAsInt();
    }

    public String getLastUpdated()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("observation_time").getAsString();
    }

    public String getFeelsLike()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("feelslike_f").getAsString();
    }

    public String getWindMph()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("wind_mph").getAsString();
    }

    public String getIcon()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("icon").getAsString();

    }

    public String getHowMuchRain()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject()
                .get("precip_today_string").getAsString();
    }

    public String getForecastDay(int v)
    {

        String b = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("date").getAsJsonObject()
                .get("weekday").getAsString();

        return b;

    }
    public String getForecastDayHigh(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("high").getAsJsonObject()
                .get("fahrenheit").getAsString();

        return a;
    }
    public String getForecastDayLow(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("low").getAsJsonObject()
                .get("fahrenheit").getAsString();
        return a;
    }
    public String getForecastDayWeather(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("conditions")
                .getAsString();
        return a;
    }
    public String getForecastDayWind(int v)
    {

        String a = jse2.getAsJsonObject().get("forecast")
                .getAsJsonObject().get("simpleforecast")
                .getAsJsonObject().get("forecastday").getAsJsonArray().get(v)
                .getAsJsonObject().get("avewind").getAsJsonObject()
                .get("mph").getAsString();
        return a;
    }

    public String getRadarURL()
    {


        String radarURL = "http://api.wunderground.com/api/ca51a08ea2deeeca/animatedradar/animatedsatellite/"
                +"q/" +getZIP()+".gif?num=6&delay=50&interval=30";

        return radarURL;

    }
    public Color backround()
    {
        double foo = Double.parseDouble(getTemperature());
        if(foo<70)
        {
            return Color.cyan;
        }
        else if(foo>70)
        {
            return Color.red;
        }
        return Color.white;
    }
    public String clothes()
    {
        double foo = Double.parseDouble(getTemperature());
        if(foo<50&&foo>0)
        {
            return "heavy jacket & pants.";
        }
        else if(foo<60 && foo> 50)
        {
            return "sweater & pants.";
        }
        else if(foo<70&&foo>60)
        {
            return "light jacket & shorts.";
        }
        else if(foo<80&&foo>70)
        {
            return "t-shirt & pants.";

        }
        else if(foo<110&&foo<80)
        {
            return "tank top & shorts.";
        }
        return "go naked";
    }
    public boolean umb()
    {
        String a = getHowMuchRain();
        String b = a.replaceAll("[^0-9]","");
        double foo = Double.parseDouble(b);
        if(foo>0) {
            return true;
        }
        return false;
    }


   /* public static void main(String[] args) {
        Core a = new Core("95747");
        System.out.println("City: "+ a.getCity());
        System.out.println("Current temp: " +a.getTemperature());
        System.out.println("Current conditions: " + a.getWeather());
        System.out.println("When last update: " +a.getLastUpdated());
        System.out.println("Current feels like: " + a.getFeelsLike());
        System.out.println("Wind MPH: " + a.getWindMph());
        System.out.println("Icon to use: " + a.getIcon());
        System.out.println("How much rain: " + a.getHowMuchRain());
        System.out.println("High: " + a.getForecastDayHigh(0));
        System.out.println("Low: " + a.getForecastDayLow(0));
        System.out.println("Weather Conditions: " + a.getForecastDayWeather(0));
        System.out.println("Certain day Wind MPH: " +a.getForecastDayWind(0));
        System.out.println("Weekday: " + a.getForecastDay(0));
        System.out.println(a.getRadarURL());

    }*/


}